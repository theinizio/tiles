package com.android.example.spinner;

import java.util.Locale;

////Набор цветов
public class Colors extends Object {
	
	public  Colors() {
		
	}
	public static final int BLUE   = 0;
	public static final int GREEN  = 1;
	public static final int RED    = 2;
	public static final int YELLOW = 3;
	public static final int PURPLE = 4;
	public static final int count  = 5;
	
	public static String   toString(int radix){
		String tmp;
		switch(radix) {
			case 0:  tmp = "Blue";   break; 
			case 1:  tmp = "Green";  break;
			case 2:  tmp = "Red";    break;
			case 3:  tmp = "Yellow"; break;
			case 4:  tmp = "Purple"; break;
			default: tmp = "unknown";break;
		}
		return tmp;
	}
	public static  int toNumber(String str) {
		     if(str.toUpperCase(Locale.ENGLISH)=="BLUE")  return BLUE;
		else if(str.toUpperCase(Locale.ENGLISH)=="GREEN") return GREEN;
		else if(str.toUpperCase(Locale.ENGLISH)=="RED")   return RED;
		else if(str.toUpperCase(Locale.ENGLISH)=="YELLOW")return YELLOW;
		else if(str.toUpperCase(Locale.ENGLISH)=="PURPLE")return PURPLE;
		else return 99;
	}
}