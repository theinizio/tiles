package com.android.example.spinner;



//import com.android.example.spinner.SpinnerActivity.UpdateTimeTask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import com.android.example.spinner.Tile.UpdateTimeTask;

import android.app.Activity;
import android.content.Context;
//import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
//import android.graphics.Point;
import android.graphics.Typeface;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
//import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class Game extends Activity{
	
	
	
	private SharedPreferences settings;
	
	
	private int tilesHorisontalCount=15;
	private int tilesVerticalCount=10;
	private int lastButtonPressedColor=1;
	
	private Map<String, Tile> tiles=new Hashtable<String, Tile>(100, (float) 0.5);
	private boolean savedInBundle=false;
	private int width=33;
	private int height=33;
	private int tileWidth;
	private int offsetX = -20;
	private int offsetY = -10;
	private long start_timer;
	private TextView movesLabel;
	private TextView scoreLabel;
	private TextView playtime;
	
	public static int animationLength=11;
	
	public static Bitmap[] tileFramesG = new Bitmap[Colors.count];
	public static Bitmap[][] tFG = new Bitmap[Colors.count][animationLength];
	
	//private int    hardness = 5;
	//private ArrayList undo_all=new ArrayList();
	//private ArrayList undo_row=new ArrayList();
	//private int    anim_width=11;
	private int    nc;
	private long    finish_timer;
	private String posname;
	private int    moves;
	
	//	private Timer  everySecondTimer;
		
	private int    newcolor;
	private int    undocolor;
	private ArrayList<Integer> ucols=new ArrayList<Integer>();
	private ArrayList<String> undo_infected=new ArrayList<String>();
	private ArrayList<ArrayList<String>> undo_items=new ArrayList<ArrayList<String>>();
	private int currentcolor;
	private ArrayList<String> infected=new ArrayList<String>();
	public long timeInpause=0;
	public long whenPaused;
	private MediaPlayer turnSound;
	private MediaPlayer finishSound;
	private ImageButton undoButton;
	private boolean paused;
	private Timer timer;
	private RelativeLayout r;
	private SQLiteDatabase db;
	private Date d=new Date();
	private Boolean showHighScore;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.v("where I am", "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.game);
        settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        init(savedInstanceState);
	}
	
	//задача для таймера
		class UpdateTimeTask extends TimerTask {
			public void run(){
				TimerMethod();
			} 
		}

		private void TimerMethod(){
			this.runOnUiThread(Timer_Tick);
		}

		private Runnable Timer_Tick = new Runnable() {

			@Override
			public void run() {
			 //double	startTMS=System.currentTimeMillis();
				if(!paused){
					//moverank(hardness,moves);
					String a=""+((Math.round((new Date().getTime() - start_timer - timeInpause) / 1000)));
					//Log.v("time", ""+new Date().getTime()+", "+start_timer+", "+timeInpause);
					playtime.setText( a);
					int movesDiff = 20000 + (20 - moves) * 1000;
					long timeDiff  =  1000 + (60-Long.parseLong(playtime.getText().toString())) * 100;
					a=""+(movesDiff + timeDiff);
					scoreLabel.setText (a);
				}
				//Log.v("Game", "timerTime="+(System.currentTimeMillis()-startTMS));
				
			}
		    
		};

	   
    private void  init(Bundle s){
    	Boolean bundleIsNull=true;
    	if(s==null)bundleIsNull=true; else bundleIsNull=false;
    	Log.v("where I am", "init "+bundleIsNull);
      //if(s==null)
      {
    	  
    	timer = new Timer();
    	start_timer=new Date().getTime();
		TimerTask updateTime = new UpdateTimeTask();
		timer.schedule(updateTime,0, 1000);
		Typeface tf=Typeface.createFromAsset(getAssets(),"Days.ttf");
		playtime  =(TextView)findViewById(R.id.playTime);
		playtime.setTypeface(tf);
		//playtime.setTextColor(0xCCCCCC);
		playtime.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
		scoreLabel=(TextView)findViewById(R.id.scorelabel);
		scoreLabel.setTypeface(tf);
		//scoreLabel.setTextColor(0xCCCCCC);
		scoreLabel.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
		movesLabel=(TextView)findViewById(R.id.movesLabel);
		movesLabel.setTypeface(tf);
		//movesLabel.setTextColor(0xCCCCCC);
		movesLabel.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
		


		Log.v("moves", "init "+moves);
		moves=0;
		movesLabel.setText("0");
		
    	tileFramesG[Colors.BLUE  ] = BitmapFactory.decodeResource(getResources(),R.drawable.bluetile);
		tileFramesG[Colors.GREEN ] = BitmapFactory.decodeResource(getResources(),R.drawable.greentile);
	    tileFramesG[Colors.RED   ] = BitmapFactory.decodeResource(getResources(),R.drawable.redtile);
	    tileFramesG[Colors.YELLOW] = BitmapFactory.decodeResource(getResources(),R.drawable.yellowtile);
	    tileFramesG[Colors.PURPLE] = BitmapFactory.decodeResource(getResources(),R.drawable.purpletile);
    	
	    width  = tileFramesG[Colors.GREEN].getWidth ();
	 	height = tileFramesG[Colors.GREEN].getHeight();
	 	
	 	
	 	tileWidth = width / (animationLength);
	 	width=tileWidth;
	 	int[] pixels = new int[tileWidth * height];
	 	//Log.v("WH", "W="+tileWidth+", H="+height);

	 	//startMS=System.currentTimeMillis();
	 	for (int z=0;z<Colors.count;z++){
			////Log.v("Game.init", "z="+Colors.toString(z));
			tFG[z]=new Bitmap[animationLength];
			for (int i=0;i<animationLength;i++){
				tFG[z][i]= Bitmap.createBitmap(tileWidth, height, Bitmap.Config.ARGB_8888);
				tileFramesG[z].getPixels(pixels, 0, tileWidth, i*tileWidth, 0, tileWidth, height);
				tFG[z][i].setPixels(pixels, 0, tileWidth, 0, 0, tileWidth, height);
			}
	    }
	    //Log.v("Speed", "pixelSetTime="+(System.currentTimeMillis()-startMS));
	   // startMS=System.currentTimeMillis();
    	turnSound   = MediaPlayer.create(this, R.raw.turn);
    	finishSound = MediaPlayer.create(this, R.raw.badend);
    	//if(SpinnerActivity.isPlaying()){SpinnerActivity.music.start();}
    	
    	
    	
    	
    	
    	BitmapFactory.Options o = new BitmapFactory.Options();
    	o.inTargetDensity = DisplayMetrics.DENSITY_DEFAULT;
    	Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.greentile, o);
    	//int width = bmp.getWidth()/animationLength;
    	//int height = bmp.getHeight();
    	Log.v("WH", "W="+width+", H="+height);
    	undoButton=(ImageButton) findViewById(R.id.undoBtn);
    	
   	 	r=(RelativeLayout) findViewById(R.id.gameLayout);
        if(infected.size()>0){
       	 infected.clear();
       	 infected=new ArrayList<String>();
        }
       	 
       	 ucols.clear();
       	//Log.v("Speed", "otherTime="+(System.currentTimeMillis()-startMS));
	    //startMS=System.currentTimeMillis();
	    
       if(tiles.size()>0){
       	 for(int y1=1;y1<=tilesVerticalCount;y1++){	       
             	for(int x1=1;x1<=tilesHorisontalCount;x1++){
             		int pos = x1 + y1 * 100;
    				String  posStr= "pos";
    				posStr += pos > 1000?pos:"0" + pos;
    				tiles.get(posStr).setImageBitmap(null);
    				tiles.get(posStr).destroyDrawingCache();
             	}
            }
       	tiles.clear();
       	tiles=new Hashtable<String, Tile>(100, (float) 0.5);
       }
       
       //Log.v("Speed", "обнуление="+(System.currentTimeMillis()-startMS));
	    //startMS=System.currentTimeMillis();
	    
	    
       for(int y1=1;y1<=tilesVerticalCount;y1++){	       
        	for(int x1=1;x1<=tilesHorisontalCount;x1++){
        		LayoutParams imageViewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT );
            	
                int pos = x1 + y1 * 100;
				 String  posStr= "pos";
				posStr += pos > 1000?pos:"0" + pos;
				//////Log.v("tag", posStr);
				tiles.put(posStr, new Tile(this));
				////Log.v("Color",posStr+".addedColor="+Colors.toString(tiles.get(posStr).getColor()));
				tiles.get(posStr).name = posStr;
				imageViewLayoutParams.leftMargin=offsetX + width * x1;
				imageViewLayoutParams.topMargin=offsetY + height * y1; 
				tiles.get(posStr).setLayoutParams(imageViewLayoutParams);
				tiles.get(posStr).setId(pos);
                r.addView(tiles.get(posStr));
                
        	}
       }
       
       //Log.v("Speed", "paramSetTime="+(System.currentTimeMillis()-startMS));
	    //startMS=System.currentTimeMillis();
	    
	    
        for (int zi = 0; zi <= 10000; zi++) Math.random();
   	
        //Log.v("Speed", "rndSetTime="+(System.currentTimeMillis()-startMS));
	    //startMS=System.currentTimeMillis(); 
        
		//moves=0;
	//	movesLabel.text=(moves)/*.toString()*/;
		undoButton.setEnabled(false);
		//uall.enabled=false;
		//ucols=new ArrayList<int>();
		//undo_items=new Array();
		infected.add("pos0101");
		
		
		
		
		
		lastButtonPressedColor=currentcolor = tiles.get(infected.get(0)).getColor();
		//tiles.get(infected.get(0)).showColor(Colors.RED);
		////Log.v("initcolor", "initcolor="+currentColor+"==="+tiles.get(infected.get(0)/*"pos0101"*/).getId());
		ucols.add(0, currentcolor);
		newcolor = currentcolor;
		checkneighbors(currentcolor);
		 //Log.v("Speed", "checkNeighboursTime="+(System.currentTimeMillis()-startMS));
		    //startMS=System.currentTimeMillis();
      }	
	}	

    private int rand(int iants /*= Colors.count*/){	
    	//Log.v("Start", "..Started:rand");
    	//double	startrMS=System.currentTimeMillis();
  		int tmp=iants+1;
  		do{
  			tmp=(int) Math.round(Math.random()*(iants));
  			}while(tmp==(iants));
  		//Log.v("Game", "randTime="+(System.currentTimeMillis()-startrMS));
  		return tmp;
  	}
    
   private int getcolor(String tile){
	  // //double	startgMS=System.currentTimeMillis();
		return tiles.get(tile).getColor();
		////Log.v("Game", "getcolorTime="+(System.currentTimeMillis()-startgMS));
		//return 0;
	}

   
   private void checkneighbors(int color){
	    String posright;
		String posdown;
		String posleft;
		String posup;
	   //Log.v("Start", "..Started:checkNeighBours");
	   //double	startcMS=System.currentTimeMillis();
		Boolean res=false;
		String item;
		for  ( int index=0; index<infected.size();index++) {
				item=(String)infected.get(index);
			
				
				if(item!="")undo_infected.add(item);
   			
				//////Log.v("neigh", "item"+item+", posname="+posname+", "+"color="+color+", "+Colors.toString(color));
				int j = Integer.parseInt(item.substring(3,5));
				int i = Integer.parseInt(item.substring(5));
				
				if ((i>=1)&&(i<=tilesHorisontalCount)&&(j>=1)&&(j<=tilesVerticalCount)){
					posname="pos"+((j)<10?("0"+j):(j))+(((i)<10)?("0"+i):i);
					////Log.v("123", "  posname="+posname+", "+Colors.toString(color)); 
				}else posname="";
					
				if ((i>=1)&&(i<=(tilesHorisontalCount-1))&&(j>=1)&&(j<=(tilesVerticalCount))){
					posright="pos"+((j)<10?("0"+j):(j))+(((i+1)<10)?("0"+(i+1)):(i+1));
					////Log.v("123", "  posright="+posright+", "+Colors.toString(tiles.get(posright).getColor()));
					
				}else {posright="";}
				
				if((i>=1)&&(i<=(tilesHorisontalCount))&&(j>=1)&&(j<=(tilesVerticalCount-1))){
					posdown="pos"+((j+1)<10?("0"+(j+1)):(j+1))+(((i)<10)?("0"+(i)):i);
					////Log.v("123", "  posdown="+posdown+", "+Colors.toString(tiles.get(posdown).getColor()));
					
				}else {posdown="";}
				
				if((i>=2)&&(i<=(tilesHorisontalCount))&&(j>=1)&&(j<=(tilesVerticalCount))){
					posleft="pos"+((j)<10?("0"+j):(j))+(((i-1)<10)?("0"+(i-1)):((i-1)));
					////Log.v("123", "  posleft="+posleft+", "+Colors.toString(tiles.get(posleft).getColor()));
					
				}else {posleft="";}
				
				if((i>=1)&&(i<=(tilesHorisontalCount))&&(j>=2)&&(j<=(tilesVerticalCount))){
					posup="pos"+((j-1)<10?("0"+(j-1)):((j-1)))+(((i)<10)?("0"+(i)):((i)));
					////Log.v("123", "  posup="+posup+", "+Colors.toString(tiles.get(posup).getColor()));
					
				}else {posup="";}
				
				
					if((posright!="")&&(posname!=""))
						if(((color)==getcolor(posright))&&(infected.indexOf(posright)==-1))
							{
								infected.add(posright);
								undo_infected.add(posright);
								res=true;
								////Log.v("123", "added "+infected.get(infected.size()-1));
							}
					
					if((posdown!="")&&(posname!=""))
						if(((color)==getcolor(posdown)) &&(infected.indexOf(posdown)==-1))
							{
								infected.add(posdown);
								undo_infected.add(posdown);
								res=true;
								////Log.v("123", "added "+infected.get(infected.size()-1));
							}
					
					if((posup!="")&&(posname!=""))
						if(((color)==getcolor(posup))&&(infected.indexOf(posup)==-1))
							{
								infected.add(posup);
								undo_infected.add(posup);
								res=true;
								////Log.v("123", "added "+infected.get(infected.size()-1));
							}
					
					if((posleft!="")&&(posname!=""))
					if(((color)==getcolor(posleft))&&(infected.indexOf(posleft)==-1))
						{
							infected.add(posleft);
							undo_infected.add(posleft);
							res=true;
							////Log.v("123", "added "+infected.get(infected.size()-1));
						}
			}
			
			////Log.v("CheckNeighbours"," adding to UI."+clear_array(undo_infected)+"|moves="+moves);
			undo_items.add(moves,clear_array(undo_infected));
			
		if(res==true) checkneighbors(color);
		//Log.v("Game", "checkneigentTime="+(System.currentTimeMillis()-startcMS));
	}
	
			
		
   
   public void showPurple(View v){
	   //Log.v("Start", "..Started:showColor");
	   //double	startpMS=System.currentTimeMillis();
   	if(lastButtonPressedColor!=Colors.PURPLE){
   		lastButtonPressedColor=Colors.PURPLE;
   		colorChangeHandler();
   	}
   	//Log.v("Game", "showTime="+(System.currentTimeMillis()-startpMS));
   }   
   public void showGreen(View v){
	   //Log.v("Start", "..Started:showColor");
	   //double	startgMS=System.currentTimeMillis();
   	if(lastButtonPressedColor!=Colors.GREEN){
   		lastButtonPressedColor=Colors.GREEN;
   		colorChangeHandler();
   	}
   	//Log.v("Game", "showTime="+(System.currentTimeMillis()-startgMS));
   }
   public void showRed(View v){
	   //Log.v("Start", "..Started:showColor");
	   //double	startrMS=System.currentTimeMillis();
   	if(lastButtonPressedColor!=Colors.RED){
   		lastButtonPressedColor=Colors.RED;
   		colorChangeHandler();
   	}
   	//Log.v("Game", "showTime="+(System.currentTimeMillis()-startrMS));
   }
   public void showYellow(View v){
	   //Log.v("Start", "..Started:showColor");
	   //double	startyMS=System.currentTimeMillis();
		if(lastButtonPressedColor!=Colors.YELLOW){
			lastButtonPressedColor=Colors.YELLOW;
			colorChangeHandler();
		}
		//Log.v("Game", "showTime="+(System.currentTimeMillis()-startyMS));
	}
	public void showBlue(View v){
		//Log.v("Start", "..Started:showColor");
		//double	startbMS=System.currentTimeMillis();
		if(lastButtonPressedColor!=Colors.BLUE){
			lastButtonPressedColor=Colors.BLUE;
			colorChangeHandler();
		}
		//Log.v("Game", "showTime="+(System.currentTimeMillis()-startbMS));
	}                         
		
		private void colorChangeHandler(){
			//Log.v("Start", "..Started:colorChangeHandler");
			//double	startcMS=System.currentTimeMillis();
			undo_infected.clear();
			if(lastButtonPressedColor!=(Integer)ucols.get(moves))
			{
				String text=(++moves)+"";
				movesLabel.setText(text);
				if (moves>0){ undoButton.setEnabled(true);};
				undocolor=currentcolor;
				newcolor=lastButtonPressedColor;			
				ucols.add(moves, newcolor);
				//String a=""+(++moves);
				//movesLabel.setText(a);
				//if (moves>0){ undoButton.setEnabled(true);};			
				for  (Object _item: infected) {	
					tiles.get(_item).showColor(lastButtonPressedColor);
				}			
				if(turnSound.isPlaying()) {
					turnSound.seekTo(0);
					////Log.v("music", "is Playing "+100.0*(turnSound.getDuration()-turnSound.getCurrentPosition())/turnSound.getDuration());
				}	
				turnSound.start();
				checkneighbors(lastButtonPressedColor);
				////Log.v("ColorChangeH"," adding to UI."+clear_array(undo_infected)+"|moves="+moves);
				undo_items.add(moves,clear_array(undo_infected));
			
				if (infected.size()>=tilesVerticalCount*tilesHorisontalCount){
					finish_timer=new Date().getTime();
					myFinish();		
				}	
			}	
			//Log.v("Game", "colorChangeHandlerTime="+(System.currentTimeMillis()-startcMS));
		}
		
		
		
   
		private ArrayList clear_array(ArrayList arr){
			//Log.v("Start", "..Started:arr_clear");
			//double	startaMS=System.currentTimeMillis();
			ArrayList newarr=new ArrayList();
			if (arr.isEmpty())return newarr;
			Collections.sort(arr);
			//arr.sort();
			for( int i=0;i<arr.size();i++){
				if (newarr.indexOf(arr.get(i))==-1)newarr.add(arr.get(i));
			}
			//Log.v("Game", "clearArrayTime="+(System.currentTimeMillis()-startaMS));
			return newarr;
		}

		private  ArrayList<String> arr_diff(ArrayList<String> ar1,ArrayList<String> ar2){ //вычисляет совпадающие элементы
			//Log.v("Start", "..Started:arr_diff");
			//double	startdMS=System.currentTimeMillis();
			ArrayList<String> result=new ArrayList<String>();
			for (String t:ar2)
				if(ar1.contains(t))result.add(t);
			//Log.v("Game", "diff_ArrayTime="+(System.currentTimeMillis()-startdMS));
			return clear_array(result);
		}

	
		public void undoHandler(View v){
			//Log.v("Start", "..Started:undoHandler");
			//double	startuMS=System.currentTimeMillis();
			ArrayList<String> workarr=new ArrayList<String>();
			String a=""+(--moves);
			movesLabel.setText(a);
			if(moves==0){undoButton.setEnabled(false);/*uall.enabled=false;*/}
			
			if (moves>=0){
				////Log.v("Undo!", ""+undo_items.toString());
				workarr=arr_diff(undo_items.get(moves),undo_items.get(moves+1));
			}else { workarr =  undo_items.get(moves);
					undoButton.setEnabled(true);
				  }
			for (Object  items :workarr){
				nc=(Integer) ucols.get(moves);
				////Log.v("UNDO","UNDO_NC="+Colors.toString(nc));
				tiles.get(items).showColor(nc);
				////Log.v("tag","   "+items+", "+Colors.toString(ucols.get(moves)));
			}
			infected=workarr;//undo_items[moves];
			
			undo_items.remove(moves);
			ucols.remove(moves+1);
			//Log.v("Game", "undoHandlerTime="+(System.currentTimeMillis()-startuMS));
		}
		
		private void showMenu(){
			/*
			if (e!=null) showHighScore = false;
			stage.removeChild(gameOverNameT);
			stage.removeChild(gameOverRankT);
			stage.removeChild(gameOverScoreT);
			stage.removeChild(submitButton);
			stage.removeChild(mainmenuButton);
			
			dispatchEvent(new Event(Game.GAME_OVER));
			*/
		}
		
		private void submitScore(){
			//Log.v("Start", "..Started:submitScore");
			//double	startsMS=System.currentTimeMillis();
		 	 String gameOverNameT="Vasya";
			//if ((gameOverNameT.length() > 0) && (gameOverNameT != "Anonymous" )){
		 	String locale = getBaseContext().getResources().getConfiguration().locale.getCountry();
		 	ManController.write(getBaseContext(), locale, gameOverNameT, Integer.parseInt(""+ scoreLabel.getText()),
						                                           Integer.parseInt(""+ movesLabel.getText()), 
								                                   Integer.parseInt(""+   playtime.getText())); 	
				
			
		//}
			showHighScore = true;
			showMenu();
			//Log.v("Game", "submitscoreTime="+(System.currentTimeMillis()-startsMS));
		}
		
		
		public void showScore(){
			/*
			dispatchEvent(new Event(REMOVE_MMB));
			
			gameOverScoreT.text = scoreLabel.text;
			gameOverNameT.text = playerName;
			stage.addChild(gameOverNameT);
			stage.focus = gameOverNameT;
			stage.addChild(submitButton);
			stage.addChild(gameOverRankT);
			stage.addChild(gameOverScoreT);
			stage.addChild(mainmenuButton);
			blinds.removeListeners();
			removeChild(pauseButton);
			
			*/
			//trace("GOTF added");
		}
		private void blindsOpenFinish(){
			//Log.v("Start", "..Started:blindsOpenFinish");
			//double	startlMS=System.currentTimeMillis();
			//removeChild(blinds);
			paused = false;
			timeInpause += (new Date().getTime() - whenPaused);
			////Log.v("blindsOpenFinish","time in pause= "+timeInpause + "\n");
			//String scoreString = ""+(Math.round(timeInpause / 1000));
			//scoreLabel.setText(scoreString);
			//everySecond();
			//Log.v("Game", "blindsopenFinishTime="+(System.currentTimeMillis()-startlMS));
		}
		

		
		public void pauseFunc(View v){
			
			//whenPaused = (new Date().getTime());
			//Log.v("pauseFunc","whenPaused="+whenPaused );
			//paused = true;
			onPause();
			
			
		}
		
	public void continueButtonHandler(View v){
		onResume();
		
	}

	private void blindsClose(){
		final Animation leftBlinds  = AnimationUtils.loadAnimation(this, R.anim.blinds_left_close );
		final Animation rightBlinds = AnimationUtils.loadAnimation(this, R.anim.blinds_right_close);
		ImageView bLeft  = (ImageView)findViewById(R.id.bLeft);
		ImageView bRight = (ImageView)findViewById(R.id.bRight);
    	bRight.bringToFront();
    	bLeft.bringToFront();
    	bRight.startAnimation(rightBlinds);
		bLeft.startAnimation(leftBlinds);
		bLeft.setVisibility(View.VISIBLE);
		bRight.setVisibility(View.VISIBLE);
	}
	
	private void blindsOpen(){
		final Animation leftBlinds  = AnimationUtils.loadAnimation(this, R.anim.blinds_left_open );
		final Animation rightBlinds = AnimationUtils.loadAnimation(this, R.anim.blinds_right_open);
		ImageView bLeft  = (ImageView)findViewById(R.id.bLeft);
		ImageView bRight = (ImageView)findViewById(R.id.bRight);
    	//bRight.bringToFront();
    	//bLeft.bringToFront();
    	bRight.startAnimation(rightBlinds);
		bLeft.startAnimation (leftBlinds);
		bLeft.setVisibility(View.INVISIBLE);
		bRight.setVisibility(View.INVISIBLE);
	}
	
	private void boxDown(int id){
		final Animation pauseBoxAnim   = AnimationUtils.loadAnimation(this, R.anim.pause_box_down);
		ImageView pauseBox  = (ImageView)findViewById(id);
		pauseBox.bringToFront();
		pauseBox.startAnimation(pauseBoxAnim);
		pauseBox.setVisibility(View.VISIBLE);
		continueButtonDown();
	}
	private void continueButtonDown(){
		final Animation btnAnim   = AnimationUtils.loadAnimation(this, R.anim.continue_button_down);
		ImageButton btn  = (ImageButton)findViewById(R.id.continueButton);
		btn.bringToFront();
		btn.startAnimation(btnAnim);
		btn.setVisibility(View.VISIBLE);
	}
	
	private void boxUp(int id){
		final Animation pauseBoxAnim   = AnimationUtils.loadAnimation(this, R.anim.pause_box_up);
		ImageView pauseBox  = (ImageView)findViewById(id);
		pauseBox.startAnimation(pauseBoxAnim);
		pauseBox.setVisibility(View.INVISIBLE);
		continueButtonUp();
	}
	private void continueButtonUp(){
		final Animation btnAnim   = AnimationUtils.loadAnimation(this, R.anim.continue_button_up);
		ImageButton btn  = (ImageButton)findViewById(R.id.continueButton);
		btn.bringToFront();
		btn.startAnimation(btnAnim);
		btn.setVisibility(View.INVISIBLE);
	}
	
	private void myFinish(){	
		SharedPreferences.Editor s = settings.edit();
    	s.putBoolean("_notFinished", false);
    	s.apply();
    	s=null;
		blindsClose();
		boxDown(R.id.gameOverBox);
		
		finishSound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.reset();
				mp.release();
				submitScore();
				//showMainScreen();
			}
		});
		finishSound.start();
		
		//Log.v("Game", "myFinishTime="+(System.currentTimeMillis()-startfMS));
	}
	@Override
	public void onBackPressed() {
		showMainScreen();
	}
	
	
	private void showMainScreen(){
		//Log.v("Start", "..Started:MyFinishFinish");
		//double	startffMS=System.currentTimeMillis();
		this.finish();
		overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
		Intent intent = new Intent(Game.this, SpinnerActivity.class);
	       startActivity(intent);
	   
		//DBScripts.getRank(gameOverRankT, int(scoreLabel.text));
		
		//everySecondTimer.stop();
		//addChild(blinds);
		//blinds.dispose();
		//blinds = null;
		//dispatchEvent(new Event(Game.GAME_OVER));
		//blinds.close(true);
	       //Log.v("Game", "myFinishFinishTime="+(System.currentTimeMillis()-startffMS));
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig){
    	super.onConfigurationChanged(newConfig);
    	Log.v("where I am", "onConfigurationChanged");
    	
    	//this.finish();
    }

    /**
     * Restores the current state of the spinner (which item is selected, and the value
     * of that item).
     * Since onResume() is always called when an Activity is starting, even if it is re-displaying
     * after being hidden, it is the best place to restore state.
     *
     * Attempts to read the state from a preferences file. If this read fails,
     * assume it was just installed, so do an initialization. Regardless, change the
     * state of the spinner to be the previous position.
     *
     * @see android.app.Activity#onResume()
  */
    @Override
    public void onResume() {
        super.onResume();
        Log.v("where I am", "onResume");
        
        if(paused){
        	blindsOpen();
    		boxUp(R.id.pauseBox);
    		timeInpause += (new Date().getTime() - whenPaused);
    		paused=false;
        }else{
        
       
        //init(null);
        }
        if(settings.getBoolean("_notFinished", false)){
        	SharedPreferences.Editor s = settings.edit();
        	s.putBoolean("_notFinished", false);
        	s.apply();
        	s=null;
        	whenPaused=settings.getLong("_whenPaused", 0);
        	timeInpause=settings.getLong("_timeInpause", 0);
        	currentcolor=settings.getInt("_currentcolor", 0);
        	ucols=getIntArrayList(settings, "_ucols");
        	undocolor=settings.getInt("_undocolor", 0);
        	moves=settings.getInt("_moves", 0);
        	newcolor=settings.getInt("_newcolor", 0);
        	posname=settings.getString("_posname", "");
        	nc=settings.getInt("_nc", 0);
        	movesLabel.setText(settings.getString("_movesLabel", ""));
        	scoreLabel.setText(settings.getString("_scoreLabel", ""));
        	playtime.  setText(settings.getString("_playtime", ""));
        	infected=getStringArrayList(settings, "_infected");
        	undo_infected=getStringArrayList(settings, "_undo_infected");
        	for(int i=0;i<=settings.getInt("_undoItemsSize", 0);i++){
        		undo_items.add(getStringArrayList(settings, "_undo_items"+i));
        	}
        	for(int y1=1;y1<=tilesVerticalCount;y1++){	       
        		for(int x1=1;x1<=tilesHorisontalCount;x1++){
        			int pos = x1 + y1 * 100;
        			String  posStr= "pos";
        			posStr += pos > 1000?pos:"0" + pos;
        			tiles.get(posStr).showColor(settings.getInt(posStr, 0));
        		}
        	}
        	undo_infected=getStringArrayList(settings,"_undo_infected");
        	infected=getStringArrayList(settings, "_infected");
        	lastButtonPressedColor=settings.getInt("_lastButtonPressedColor", 0);
        	currentcolor=settings.getInt("_currentcolor", 0);
        }

              
  

    }

    /*
     * Store the current state of the spinner (which item is selected, and the value of that item).
     * Since onPause() is always called when an Activity is about to be hidden, even if it is about
     * to be destroyed, it is the best place to save state.
     *
     * Attempt to write the state to the preferences file. If this fails, notify the user.
     *
     * @see android.app.Activity#onPause()
     */
    
    private void putIntArrayList(SharedPreferences.Editor s, String token, ArrayList<Integer> a){
    	if(a!=null){
    		int i = 0;
    		for(int item:a){
    			s.putInt(token+i++, item);
    		}
    		s.putInt(token+"size", i);
    	}
    }
    
    private void putStringArrayList(SharedPreferences.Editor s, String token, ArrayList<String> a){
    	if(a!=null){
    		int i = 0;
    		for(String item:a){
    			s.putString(token+i++, item);
    		}
    		s.putInt(token+"size", i);
    	}
    }
    
    private ArrayList<Integer> getIntArrayList(SharedPreferences s, String token){
    	ArrayList<Integer> a=new ArrayList<Integer>();
    	
    	for(int i=0;i<=s.getInt(token+"size",0);i++){
    		a.add(s.getInt(token+1, 0));
    	}
    	return a;
    }
    private ArrayList<String> getStringArrayList(SharedPreferences s, String token){
    	ArrayList<String> a=new ArrayList<String>();
    	
    	for(int i=0;i<=s.getInt(token+"size",0);i++){
    		a.add(s.getString(token+1, ""));
    	}
    	return a;
    }
    
    @Override
    public void onStop() {
        super.onStop();
        Log.v("where I am","onStop");
        
        
        if(!paused){
			SharedPreferences.Editor s = settings.edit();
			if(infected.size()<tilesVerticalCount*tilesHorisontalCount){
				s.putBoolean("_notFinished", true);
			}
			//editor.putInt(APP_PREFERENCES_COUNTER, mCounter);
			s.putLong("_whenPaused", whenPaused);
			s.putLong("_timeInpause", timeInpause);
			s.putInt("_currentcolor", currentcolor);
			putIntArrayList(s,"_ucols", ucols);
			s.putInt("_undocolor", undocolor);
			s.putInt("_moves", moves);
			s.putInt("_newcolor", newcolor);
			s.putString("_posname", posname);
			s.putInt("_nc", nc);
			s.putString("_movesLabel",(String) movesLabel.getText());
			
			s.putString("_scoreLabel",(String) scoreLabel.getText());
			s.putString("_playtime"  ,(String) playtime.  getText());
			putStringArrayList(s,"_infected", infected);
			putStringArrayList(s,"_undo_infected", undo_infected);
			r=(RelativeLayout) findViewById(R.id.gameLayout);
			int i=0;
			for (ArrayList<String>  items :undo_items){
				putStringArrayList(s,"_undo_items"+i++, items);
			}
			for(int y1=1;y1<=tilesVerticalCount;y1++){	       
	        	for(int x1=1;x1<=tilesHorisontalCount;x1++){
	        		int pos = x1 + y1 * 100;
					String  posStr= "pos";
					posStr += pos > 1000?pos:"0" + pos;
					s.putInt(posStr,tiles.get(posStr).getColor());
					tiles.get(posStr).finish();
					tiles.get(posStr).setImageBitmap(null);
					tiles.get(posStr).destroyDrawingCache();
					r.removeView(tiles.get(posStr));
	         	}
	        }
			tiles.clear();
			tiles=new Hashtable<String, Tile>(100, (float) 0.5);
			s.putInt("_undoItemsSize", i);
			s.putInt("_lastButtonPressedColor", lastButtonPressedColor);
			s.putInt("_currentcolor", currentcolor);
			s.apply();
			s=null;
			timer.purge();
			timer.cancel();
        }
	    //s.apply();
	      
	   
        
        
    }
    
    @Override
    public void onRestart() {
        super.onRestart();
        Log.v("where I am","onRestart");
    }
    
    
    
    @Override
    public void onStart() {
        super.onStart();
        Log.v("where I am","onStart");
    }
    
    
    @Override
    public void onPause() {
        super.onPause();
        
        Log.v("where I am","onPause");
    	whenPaused =  new Date().getTime();
		
		
		paused = true;
		blindsClose();
		boxDown(R.id.pauseBox);      
		
		/*
		SharedPreferences.Editor s = settings.edit();
		//editor.putInt(APP_PREFERENCES_COUNTER, mCounter);
		s.putLong("_whenPaused", whenPaused);
		s.putLong("_timeInpause", timeInpause);
		s.putInt("_currentcolor", currentcolor);
		putIntArrayList(s,"_ucols", ucols);
		s.putInt("_undocolor", undocolor);
		s.putInt("_moves", moves);
		s.putInt("_newcolor", newcolor);
		s.putString("_posname", posname);
		s.putInt("_nc", nc);
		s.putString("_movesLabel",(String) movesLabel.getText());
		
		s.putString("_scoreLabel",(String) scoreLabel.getText());
		s.putString("_playtime"  ,(String) playtime.  getText());
		putStringArrayList(s,"_infected", infected);
		putStringArrayList(s,"_undo_infected", undo_infected);
		int i=0;
		for (ArrayList<String>  items :undo_items){
			putStringArrayList(s,"_undo_items"+i++, items);
		}
		for(int y1=1;y1<=tilesVerticalCount;y1++){	       
        	for(int x1=1;x1<=tilesHorisontalCount;x1++){
        		int pos = x1 + y1 * 100;
				String  posStr= "pos";
				posStr += pos > 1000?pos:"0" + pos;
				s.putInt(posStr,tiles.get(posStr).getColor());
        	}
		}
		s.putInt("_undoItemsSize", i);
		s.putInt("_lastButtonPressedColor", lastButtonPressedColor);
		s.putInt("_currentcolor", currentcolor);
		
	    //s.apply();
	     * 
	     */
    }
    
    
    	@Override
    	public void onSaveInstanceState(Bundle s) {
    		super.onSaveInstanceState(s);
    		
    		Log.v("where I am","onSaveInstanceState");
    		paused=true;
    		s.putLong("_start_timer",start_timer);
    		s.putLong("_whenPaused", whenPaused);
    		s.putLong("_timeInpause", timeInpause);
    		s.putInt("_currentcolor", currentcolor);
    		s.putIntegerArrayList("_ucols", ucols);
    		s.putInt("_undocolor", undocolor);
    		s.putInt("_moves", moves);
    		s.putInt("_newcolor", newcolor);
    		s.putString("_posname", posname);
    		s.putInt("_nc", nc);
    		s.putCharSequence("_movesLabel", movesLabel.getText());
    		s.putCharSequence("_scoreLabel", scoreLabel.getText());
    		s.putCharSequence("_playtime"  , playtime.  getText());
    		s.putStringArrayList("_infected", infected);
    		s.putStringArrayList("_undo_infected", undo_infected);
    		int i=0;
    		for (ArrayList<String>  items :undo_items){
    			s.putStringArrayList("_undo_items"+i++, items);
    		}
    		for(int y1=1;y1<=tilesVerticalCount;y1++){	       
            	for(int x1=1;x1<=tilesHorisontalCount;x1++){
            		int pos = x1 + y1 * 100;
    				String  posStr= "pos";
    				posStr += pos > 1000?pos:"0" + pos;
    				s.putInt(posStr,tiles.get(posStr).getColor());
    				tiles.get(posStr).finish();
    				tiles.get(posStr).setImageBitmap(null);
    				tiles.get(posStr).destroyDrawingCache();
    				r.removeView(tiles.get(posStr));
             	}
            }
    		tiles.clear();
    		tiles=new Hashtable<String, Tile>(100, (float) 0.5);
    		s.putInt("_undoItemsSize", i);
    		s.putInt("_lastButtonPressedColor", lastButtonPressedColor);
    		s.putInt("_currentcolor", currentcolor);
    		
    		
    	}
    

    	@Override
    	public void onRestoreInstanceState(Bundle s) {
    		super.onRestoreInstanceState(s);
    		Log.v("where I am","onRestoreInstanceState");
    		init(s);
    		start_timer=s.getLong("_start_timer");
    		whenPaused=s.getLong("_whenPaused");
    		timeInpause=s.getLong("_timeInpause");
    		currentcolor=s.getInt("_currentcolor");
    		ucols=s.getIntegerArrayList("_ucols");
    		undocolor=s.getInt("_undocolor");
    		Log.v("moves", "before s "+moves);
    		moves=s.getInt("_moves");
    		Log.v("moves", "after s "+moves);
    		newcolor=s.getInt("_newcolor");
    		posname=s.getString("_posname");
    		nc=s.getInt("_nc");
    		movesLabel.setText(s.getCharSequence("_movesLabel"));
    		scoreLabel.setText(s.getCharSequence("_scoreLabel"));
    		playtime.  setText(s.getCharSequence("_playtime"  ));
    		infected=s.getStringArrayList("_infected");
    		undo_infected=s.getStringArrayList("_undo_infected");
    		for(int i=0;i<=s.getInt("_undoItemsSize");i++){
    			undo_items.add(s.getStringArrayList("_undo_items"+i));
    		}
    		for(int y1=1;y1<=tilesVerticalCount;y1++){	       
            	for(int x1=1;x1<=tilesHorisontalCount;x1++){
            		int pos = x1 + y1 * 100;
    				 String  posStr= "pos";
    				posStr += pos > 1000?pos:"0" + pos;
    				tiles.get(posStr).showColor(s.getInt(posStr));
            	}
    		}
    				
    		
    		undo_infected=s.getStringArrayList("_undo_infected");
    		infected=s.getStringArrayList("_infected");
    		lastButtonPressedColor=s.getInt("_lastButtonPressedColor");
    		currentcolor=s.getInt("_currentcolor");
    		
    	}
	}
