package com.android.example.spinner;



import com.android.example.spinner.DatabaseContract.Names;
import com.android.example.spinner.DatabaseContract.Names.NamesColumns;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class HighScores extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
		
        /**
         * derived classes that use onCreate() overrides must always call the super constructor
         */
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.highscores);  
        DatabaseOpenHelper dbhelper = new DatabaseOpenHelper(getBaseContext());
        SQLiteDatabase sqliteDB = dbhelper.getWritableDatabase();
        String position=""+1;
        final String[] from = { "_id", NamesColumns.NAME, NamesColumns.SCORE };
        final Cursor c = sqliteDB.query(Names.TABLE_NAME, null, null, null, null, null, Names.DEFAULT_SORT + " LIMIT 0,5");
        //Log.v("SQL","cursor="+c.getCount());
        //final int i = c.getCount();
        final int[] to = new int[] { R.id.posttion, R.id.name, R.id.score };
        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.test, c, from, to);
        final ListView lv = (ListView) findViewById(R.id.listView1);            
        lv.setAdapter(adapter);
        
       
        dbhelper.close();
        sqliteDB.close();
}
        
	private void buttonHandler(){
		this.finish();   
	    overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out); 
	    Intent intent = new Intent(HighScores.this, SpinnerActivity.class);
	       startActivity(intent);
	}
	@Override
	public void onBackPressed() {
		buttonHandler();
	}
	
	public void onBackPressed(View v) {
		buttonHandler();
	}
}
