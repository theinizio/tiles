/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.spinner;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;

import java.util.TimerTask;

import com.android.example.spinner.R;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.content.Context;
//import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;






/**
 * Displays an Android spinner widget backed by data in an array. The
 * array is loaded from the strings.xml resources file.
 */
 public class SpinnerActivity extends Activity {
	public static MediaPlayer music;
	private static Boolean musicIsPlaying=true;
	public int timeInpause;
	public int whenPaused;
	private ImageView bLeft;
	private ImageView bRight;
	private SharedPreferences settings;
	
	    @Override
    public void onCreate(Bundle savedInstanceState) {

        /**
         * derived classes that use onCreate() overrides must always call the super constructor
         */
        super.onCreate(savedInstanceState);
        
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        settings = getSharedPreferences("settings", Context.MODE_PRIVATE);

        if(settings.getBoolean("_notFinished", false) ){
        	openGameAvtivity(null);
        	Log.v("Main", "not finished game? going into!");
        }else{ setContentView(R.layout.main);
        
        Typeface tf=Typeface.createFromAsset(getAssets(),"Days.ttf");
		Button b =(Button)findViewById(R.id.button1);
		b.setTypeface(tf);
		//playtime.setTextColor(0xCCCCCC);
		b.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        }
    	    		//bLeft.setImageResource(R.drawable.blinds);
    		//bRight.setImageResource(R.drawable.blinds2);

    		
    		//bLeft.setLayoutParams(imageViewLayoutParamsLeft);
    		//bRight.setLayoutParams(imageViewLayoutParamsRight);
    		//r.addView(bLeft);
            //r.addView(bRight);
        
        
       // music = MediaPlayer.create(this, R.raw.bg);
      
        /*music.setOnCompletionListener(   new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
               
               // mp.start();
                musicIsPlaying=true;
            }
        });*/
       // music.start();
       // musicIsPlaying=true;
        
    }
    
   public static Boolean isPlaying(){
	   if(musicIsPlaying){return true;}
	   else return false;
   }
    
   public void toggleSound(View v){
	   ImageButton soundBtn= (ImageButton)findViewById(R.id.soundBtn);
	   if(music.isPlaying()){
		   soundBtn.setImageResource(R.drawable.soundon);
		   //music.pause();
		   musicIsPlaying=false;
	   }else{
		   soundBtn.setImageResource(R.drawable.soundoff);
		   //music.start();
		   musicIsPlaying=true;
	   }
   }
	
   
   
    public void openGameAvtivity(View view){
	   this.finish(); 
	     
       overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out); 
	   Intent intent = new Intent(SpinnerActivity.this, Game.class);
       startActivity(intent);
   }

  public void openHow2PLay(View v){

	  this.finish(); 
      overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out); 
	  Intent intent = new Intent(SpinnerActivity.this, How2PLay.class);
      startActivity(intent);
      
  }
  
  public void openHighscores(View v){

	  this.finish(); 
      overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out); 
	  Intent intent = new Intent(SpinnerActivity.this, HighScores.class);
      startActivity(intent);
      
  }
 
   
    @Override
    public void onConfigurationChanged(Configuration newConfig){
    	super.onConfigurationChanged(newConfig);
    	musicIsPlaying=false;
    	//music.pause();
    }
    
    /**
     * Restores the current state of the spinner (which item is selected, and the value
     * of that item).
     * Since onResume() is always called when an Activity is starting, even if it is re-displaying
     * after being hidden, it is the best place to restore state.
     *
     * Attempts to read the state from a preferences file. If this read fails,
     * assume it was just installed, so do an initialization. Regardless, change the
     * state of the spinner to be the previous position.
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    public void onResume() {

        /*
         * an override to onResume() must call the super constructor first.
         */

        super.onResume();

        /*
         * Try to read the preferences file. If not found, set the state to the desired initial
         * values.
         */
        //if(musicIsPlaying)
        	//music.start();

        /*
         * Set the spinner to the current state.
         */

       // Spinner restoreSpinner = (Spinner)findViewById(R.id.Spinner01);
        //restoreSpinner.setSelection(getSpinnerPosition());

    }

    /**
     * Store the current state of the spinner (which item is selected, and the value of that item).
     * Since onPause() is always called when an Activity is about to be hidden, even if it is about
     * to be destroyed, it is the best place to save state.
     *
     * Attempt to write the state to the preferences file. If this fails, notify the user.
     *
     * @see android.app.Activity#onPause()
     */
    @Override
    public void onPause() {

        /*
         * an override to onPause() must call the super constructor first.
         */
    	

        super.onPause();
        musicIsPlaying=false;
        //music.pause();
        /*
         * Save the state to the preferences file. If it fails, display a Toast, noting the failure.
         */

        
    }

    /**
     * Sets the initial state of the spinner when the application is first run.
     */
    public void setInitialState() {

        //this.mPos = DEFAULT_POSITION;

    }

    
    

}
