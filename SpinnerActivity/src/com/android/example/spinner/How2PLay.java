package com.android.example.spinner;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class How2PLay extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {

        /**
         * derived classes that use onCreate() overrides must always call the super constructor
         */
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.how2play);  
       
        
    }
	@Override
	public void onBackPressed() {
		this.finish();   
	    overridePendingTransition(R.anim.slide_left_in, R.anim.slide_left_out); 
	    Intent intent = new Intent(How2PLay.this, SpinnerActivity.class);
	       startActivity(intent);
	}
}
