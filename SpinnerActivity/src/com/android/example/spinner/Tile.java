package com.android.example.spinner;

import java.util.Timer;
import java.util.TimerTask;

//import com.android.example.spinner.Game.UpdateTimeTask;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;


public class Tile extends ImageView {

	private Activity cont;
	private Boolean animationEnabled=false;
	private int currentAnimationFrame=5;
	
	private int currentColor=Colors.GREEN;
	private int newColor;
	private int tmpColor=1;
	private Tile tile;
	public String name; 
	private Timer timer;
	private Handler handler;
 	

	public  Tile(Context context) {
		super(context);
		currentColor = rand(Colors.count);
		cont=(Activity)context;
		tile=this;
		
		timer = new Timer();
		TimerTask updateTime = new UpdateTimeTask();
		timer.schedule(updateTime,0, 50);
		//startMS=System.currentTimeMillis();
		tile.setImageBitmap(Game.tFG[currentColor][5]);
		//Log.v("Tile", "setImageBitmapTime="+(System.currentTimeMillis()-startMS));
		
		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				setImageBitmap((Bitmap)msg.obj);
			}
		};
	}
	
	
	public void showColor(int color){
		 if ((0 <= color)&&(color < Colors.count)&&(color!=currentColor)){
			 animationEnabled=true;
			 currentAnimationFrame=0;
			 newColor=color;
		}   
	}

	public int getColor(){
		return currentColor;
	}

	private int rand(int iants /*= Colors.count*/){	
		int tmp=iants+1;
		do{
			tmp=(int) Math.round(Math.random()*(iants));
		}while(tmp==(iants));
		return tmp;
	}


	//задача для таймера
	class UpdateTimeTask extends TimerTask {
		public void run(){
			animationFrame();
		} 
	}

	
public void animationFrame(){
	if (animationEnabled){
		if(currentAnimationFrame ==  0) tmpColor    = newColor;   
		currentAnimationFrame++;
		Message msg = new Message();
		//Handler handler = new Handler(cont.getMainLooper());
		if(currentAnimationFrame<6){
			msg.obj = Game.tFG[currentColor][currentAnimationFrame+5];
			
			
			
		}else{    
			msg.obj = Game.tFG[    tmpColor][currentAnimationFrame-5];
			
			
			
			
		}
		handler.sendMessage(msg);
		if(currentAnimationFrame==10){
			currentColor= tmpColor;
			currentAnimationFrame=0;
			animationEnabled=false;
		}
	}
}
	
 public void finish(){
	cont=null;
	tile=null;
	animationEnabled=false;
	timer.cancel();
	timer.purge();
	//SpinnerActivity.music.pause();
	//  this.finish();
 }
	
}
