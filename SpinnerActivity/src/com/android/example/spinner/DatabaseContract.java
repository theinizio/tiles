package com.android.example.spinner;


import android.provider.BaseColumns;
public class DatabaseContract {

        /** Describes History Table and model. */
        public static class Names {

                /** Default "ORDER BY" clause. */
                //сортируем по фамилии в убывающем порядке
                public static final String DEFAULT_SORT = NamesColumns.SCORE + " DESC";
                //имя таблицы
                public static final String TABLE_NAME = "highscores";
                private String country; 
                //поле имя
                private String name;
                //наш счет
                private int score;
                //ходы
                private int moves;
                //и время игры
                private int time;
                
                //
                // Ниже идут сетеры и гетеры для захвата данных из базы
                //
                public String getCountry() {

                    return country;
            }

                public String getName() {

                        return name;
                }

                public int getScore() {

                        return score;
                }

                public int getMoves() {

                        return moves;
                }

                public int getTime() {

                        return time;
                }

                public void setCountry(String country) {

                    this.country = country;
            }

                public void setName(String name) {

                        this.name = name;
                }

                public void setScore(int score) {

                        this.score = score;
                }

                public void setMoves(int moves) {

                        this.moves = moves;
                }

                public void setTime(int time) {

                        this.time = time;
                }

                /*
                 * (non-Javadoc)
                 * 
                 * @see java.lang.Object#toString()
                 */
                @Override
                public String toString() {

                        StringBuilder builder = new StringBuilder();
                        builder.append(moves);
                        return builder.toString();
                }

                //Класс с именами наших полей в базе
                public class NamesColumns implements BaseColumns {
                		
                		/** Strings */
                    	public static final String COUNTRY = "country";
                        /** Strings */
                        public static final String NAME = "name";
                        /** String */
                        public static final String SCORE = "score";
                        /** String */
                        public static final String MOVES = "moves";
                        /** String */
                        public static final String TIME = "time";
                }
        }
}